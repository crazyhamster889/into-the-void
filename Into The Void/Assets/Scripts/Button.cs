﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class Button : MonoBehaviour, IPointerDownHandler
{
    public UnityEvent onDrag;

    public void OnPointerDown(PointerEventData eventData)
    {
        onDrag.Invoke();
    }
}
