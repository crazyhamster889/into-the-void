﻿using UnityEngine.Events;

public class Gas : CelestialBody
{
    [System.Serializable]
    public class GasStage
    {
        public UnityEvent action = null;
        public int size = 0;
    }

    public GasStage[] gasStages = null;

    void Update()
    {
        ChooseStage();
    }

    void ChooseStage()
    {
        if (stage < gasStages.Length - 1)
        {
            if (RB.mass > gasStages[stage + 1].size)
            {
                stage++;
                gasStages[stage].action.Invoke();
            }
        }
    }

    public void ExplosionForce(float force)
    {
        int length = GameManager.celestialBodys.Count;

        for (int i = 0; i < length; i++)
        {
            if (GameManager.celestialBodys[i] != this)
            {
                GameManager.celestialBodys[i].RB.AddExplosionForce(force, transform.position, 100);
            }
        }
    }
}
