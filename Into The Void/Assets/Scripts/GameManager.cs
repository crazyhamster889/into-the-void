﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static List<CelestialBody> celestialBodys = new List<CelestialBody>();
    public static GameManager instance;
    public static float delta;

    private float _lifetime;
    private float _width;
    private bool _toggle;

    private void Start()
    {
        instance = this;
        Refresh();
    }

    public void Refresh()
    {
        celestialBodys.Clear();

        foreach (CelestialBody celestialBody in FindObjectsOfType<CelestialBody>())
        {
            celestialBody.lifetime = _lifetime;
            celestialBody.TR.enabled = _toggle;
            celestialBodys.Add(celestialBody);
        }
    }

    void Update()
    {
        delta = Time.deltaTime / Time.timeScale;
        CallTicks();
    }

    public void SetToggleTrails(bool toggle)
    {
        _toggle = toggle;

        foreach (CelestialBody celestialBody in celestialBodys)
        {
            celestialBody.TR.enabled = toggle;
        }
    }

    public void SetTrailSize(float width)
    {
        _width = width;

        foreach (CelestialBody celestialBody in celestialBodys)
        {
            celestialBody.TR.startWidth = width;
        }
    }

    public void SetTrailLifetime(float lifetime)
    {
        _lifetime = lifetime;

        foreach (CelestialBody celestialBody in celestialBodys)
        {
            celestialBody.lifetime = lifetime;
        }
    }

    void CallTicks()
    {
        int length = celestialBodys.Count;

        for (int i = 0; i < length; i++)
        {
            celestialBodys[i].Tick();
        }
    }

    public static void DeRegisterCelestialBody(CelestialBody celestialBody)
    {
        celestialBodys.Remove(celestialBody);
    }
}
