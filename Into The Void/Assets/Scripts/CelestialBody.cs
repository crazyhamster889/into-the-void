﻿using UnityEngine;
using SpaceGraphicsToolkit;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(TrailRenderer))]
[RequireComponent(typeof(SgtSpacetimeWell))]
public class CelestialBody : MonoBehaviour
{
    public GameObject impactExplosion;

    public float spaceTimeWellRadiusMultiplier = 2;
    public float spaceTimeWellStrengthMultiplier = 0.04f;
    public float massMultiplier = 1f;

    [HideInInspector]
    public Rigidbody RB;
    [HideInInspector]
    public TrailRenderer TR;
    [HideInInspector]
    public SphereCollider SC;
    [HideInInspector]
    public SgtSpacetimeWell SSTW;
    [HideInInspector]
    public Vector3 targetScale;
    [HideInInspector]
    public float lifetime;

    protected int stage;

    void Awake()
    {
        RB = GetComponent<Rigidbody>();
        TR = GetComponent<TrailRenderer>();
        SC = GetComponent<SphereCollider>();
        SSTW = GetComponent<SgtSpacetimeWell>();

        targetScale.y = transform.localScale.y + Random.Range(0,0.1f);
        targetScale.x = targetScale.y;
        targetScale.z = targetScale.y;
    }

    public void Tick()
    {
        SetWellValues();
        SetTrailLifetime();
        Scale();
        Gravity();
    }

    private void SetWellValues()
    {
        SSTW.Strength = RB.mass * spaceTimeWellStrengthMultiplier;
        SSTW.Radius = RB.mass * spaceTimeWellRadiusMultiplier;
    }

    void SetTrailLifetime()
    {
        float trailLifetime = lifetime * Time.timeScale;
        TR.time = trailLifetime;
    }

    void Scale()
    {
        RB.mass = transform.localScale.x * massMultiplier;
        transform.localScale = Vector3.Lerp(transform.localScale, targetScale, GameManager.delta);
    }

    void Gravity()
    {
        int length = GameManager.celestialBodys.Count;

        for (int i = 0; i < length; i++)
        {
            if (GameManager.celestialBodys[i] != this)
            {
                Rigidbody celestialBodysRigidbody = GameManager.celestialBodys[i].RB;
                Transform celestialBodysTrans = GameManager.celestialBodys[i].transform;

                float M = (RB.mass * celestialBodysRigidbody.mass);
                float D = Vector3.Distance(transform.position, celestialBodysTrans.position);
                float F = M / (D * D);

                if (D > 0) {
                    RB.AddForce(-(transform.position - celestialBodysTrans.position) * F);
                }
            }
        }
    }

    public virtual void OnCollisionEnter (Collision collision)
    {
        try
        {
            Transform celestialBodyTrans = collision.transform;
            CelestialBody celestialBody = collision.transform.GetComponent<CelestialBody>();

            if (RB.mass < celestialBody.RB.mass)
            {
                Destroy(gameObject);
            }
            else
            {
                if (impactExplosion != null)
                {
                    GameObject explosion = Instantiate(impactExplosion, collision.GetContact(0).point, Quaternion.identity) as GameObject;

                    explosion.transform.localScale = celestialBodyTrans.localScale;

                    Destroy(explosion, 1);
                }

                targetScale = targetScale + celestialBody.targetScale;
            }
        } catch { }
    }

    private void OnDisable()
    {
        GameManager.DeRegisterCelestialBody(this);
    }
}