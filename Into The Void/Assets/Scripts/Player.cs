﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public Camera playerCamera = null;

    [Space]
    [Header("Movement")]

    public KeyCode rotateKey = KeyCode.Mouse0;
    public KeyCode moveKey = KeyCode.Mouse1;

    public float speed = 1.0f;
    public float pullSpeed = 1.0f;
    private float zoomSpeed = 10.0f;
    public float Sensitivity = 5.0f;

    [Space]
    [Header("Raycasting")]

    public LayerMask layerMask = Physics.AllLayers;

    public float startingDistance = 10.0f;
    public float rayLength = 10000.0f;
    public float rayRadius = 1.0f;

    private bool isHolding = false;

    private CelestialBody heldCelestialBody = null;
    private Vector3 mousePos = Vector3.zero;

    private float scroll = -11;
    private float distance = 5.0f;

    void Update()
    {
        MousePos();
        PickUp(mousePos);
        CheckReset(mousePos);
        Holding(mousePos);
        Pull();

        if (!isHolding)
            Move();
    }

    private void Pull()
    {
        distance += Input.GetAxis("Vertical") * pullSpeed;
    }

    private void MousePos()
    {
        mousePos = Input.mousePosition;
        mousePos.z = distance;
    }

    private void PickUp(Vector3 mousePos)
    {
        RaycastHit hit;
        Ray objectPos = Camera.main.ScreenPointToRay(mousePos);

        if (SphereCast (out hit, objectPos) && !isHolding && !Input.GetKey(rotateKey))
        {
            heldCelestialBody = hit.transform.GetComponent<CelestialBody>();
            distance = Vector3.Distance(playerCamera.transform.position, hit.transform.position);
        }
    }

    private void CheckReset(Vector3 mousePos)
    {
        RaycastHit hit;
        Ray objectPos = Camera.main.ScreenPointToRay(mousePos);

        if (!SphereCast(out hit, objectPos) && heldCelestialBody != null && !isHolding)
        {
            heldCelestialBody = null;
        }
    }

    private bool SphereCast(out RaycastHit hit, Ray objectPos)
    {
        return Physics.SphereCast(objectPos, rayRadius, out hit, rayLength, layerMask);
    }

    void Holding(Vector3 mousePos)
    {
        if (Input.GetKey(KeyCode.Mouse0) && heldCelestialBody != null)
        {
            Transform heldCelestialBodyTransform = heldCelestialBody.transform;
            Vector3 objectPos = Camera.main.ScreenToWorldPoint(mousePos);

            isHolding = true;
            heldCelestialBodyTransform.position = objectPos;
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            distance = startingDistance;
            isHolding = false;
        }
    }

    public void Instantiate(GameObject Obj)
    {
        distance = startingDistance;
        GameObject _Obj = Instantiate(Obj, Camera.main.ScreenToWorldPoint(mousePos), Quaternion.identity);
        heldCelestialBody = _Obj.GetComponent<CelestialBody>();
    }

    private void Move()
    {
        scroll += Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;

        Vector3 scrollVector = new Vector3(0, 0, scroll);

        playerCamera.transform.localPosition = scrollVector;

        if (Input.GetKey(rotateKey))
        {    
            Vector3 delta = new Vector3(Input.GetAxis("Mouse X") * Sensitivity, Input.GetAxis("Mouse Y") * Sensitivity, 0);

            transform.eulerAngles += new Vector3(-delta.y, delta.x, 0);
        }

        if (Input.GetKey(moveKey))
        {
            Vector3 movement = new Vector3(-Input.GetAxis("Mouse X") * speed, -Input.GetAxis("Mouse Y") * speed, 0);

            transform.Translate(movement);
        }
    }
}
