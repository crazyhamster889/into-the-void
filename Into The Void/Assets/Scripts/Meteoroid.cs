﻿using UnityEngine;
using UnityEngine.Events;

public class Meteoroid : CelestialBody
{
    [System.Serializable]
    public class MeteoroidStage
    {
        public UnityEvent action = null;
        public int size = 0;
    }

    public float water = 0.0f;

    public MeteoroidStage[] meteoroidStages = null;

    private void Start()
    {
        SetWaterLevel();
    }

    void Update()
    {
        ChooseStage();
    }

    void ChooseStage()
    {
        if (stage < meteoroidStages.Length - 1)
        {
            if (RB.mass > meteoroidStages[stage + 1].size)
            {
                stage++;
                meteoroidStages[stage].action.Invoke();
            }
        }
    }

    public override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);

        try
        {
            float collisionWater = collision.transform.GetComponent<Meteoroid>().water;
            float collisionScale = collision.transform.localScale.magnitude;
            float localScaleMagnitude = transform.localScale.magnitude;
            water += collisionWater * collisionScale / localScaleMagnitude;
            SetWaterLevel();
        }
        catch { }
    }

    public void SetWaterLevel()
    {
        foreach (MeshRenderer renderer in GetComponentsInChildren<MeshRenderer>(true))
        {
            renderer.material.SetFloat("_WaterLevel", water);
        }
    }
}
