﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class UI_Manager : MonoBehaviour
{
    public GameObject crossHair;
    public GameObject escapeMenu;
    public GameObject toolBox;

    public bool MainMenu;

    private bool isEscapeMenuActive;
    private bool isToolBoxActive;

    private void ToggleEscapeMenu()
    {
        if (escapeMenu != null)
        {
            isEscapeMenuActive = !isEscapeMenuActive;

            crossHair.SetActive(!isEscapeMenuActive);
            escapeMenu.SetActive(isEscapeMenuActive);
        }
    }

    public void LoadLevel(int level)
    {
        SceneManager.LoadScene(level);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleEscapeMenu();
        }
        if (Input.GetKeyDown(KeyCode.Tab) && !isEscapeMenuActive)
        {
            ShowToolBox();
        }
    }

    private void ShowToolBox()
    {
        isToolBoxActive = !isToolBoxActive;
        toolBox.SetActive(isToolBoxActive);
    }
}