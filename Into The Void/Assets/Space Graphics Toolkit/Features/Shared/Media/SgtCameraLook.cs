﻿using UnityEngine;

namespace SpaceGraphicsToolkit
{
	[HelpURL(SgtHelper.HelpUrlPrefix + "SgtCameraLook")]
	[AddComponentMenu(SgtHelper.ComponentMenuPrefix + "Camera Look")]
	public class SgtCameraLook : MonoBehaviour
	{
        public float speed = 5.0f; 
        public float Sensitivity = 0.1f;

		public KeyCode rotateKey = KeyCode.Mouse0;
        public KeyCode moveKey = KeyCode.Mouse1;

        [System.NonSerialized]
		private Quaternion remainingDelta = Quaternion.identity;

		protected virtual void Update()
		{
		}

		private void Move()
		{
            if (Input.GetKey(rotateKey))
            {
                Vector3 delta = new Vector3(Input.GetAxis("Mouse X") * Sensitivity, Input.GetAxis("Mouse Y") * Sensitivity, 0);

                transform.eulerAngles += new Vector3(-delta.y, delta.x, 0);
            }

            if (Input.GetKey(moveKey))
            {
                Vector3 movement = new Vector3(-Input.GetAxis("Mouse X") * speed, -Input.GetAxis("Mouse Y") * speed, 0);

                transform.Translate(movement);
            }
        }

        public void SetSensitivity(float sensitivity)
        {
            Sensitivity = sensitivity;
        }
	}
}